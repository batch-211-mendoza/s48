// Mock Database
let posts = [];

// Post ID
let count = 1;

// Add post

document.querySelector("#form-add-post").addEventListener("submit", (event) => {
	
	event.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});
	count++;
	console.log(posts);
	alert("Successfully added!");
	// invoke a function to show post later
	showPosts();
});

//Edit post
const showPosts = () => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	});
	console.log(postEntries);
	document.querySelector("#div-post-entries").innerHTML = postEntries;
};

// Update post

document.querySelector("#form-edit-post").addEventListener("submit", (event) => {

	event.preventDefault();

	for(let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			showPosts(posts);
			alert("Successfully updated!")
			break;
		}
	}

});

// Edit Post Button

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
}

// --- ACTIVITY S48 --- //

// Delete Post Button

const deletePost = (id) => {

	let selectId = document.querySelector("#txt-edit-id").value = id;

	posts.forEach((post) => {
		if(post.id === parseInt(selectId)){
			let indexOfSelectedId = posts.indexOf(post);
			posts.splice(indexOfSelectedId, 1);
			showPosts(posts);
		}
	});
}